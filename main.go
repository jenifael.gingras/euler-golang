package main

import "fmt"
// import "gitlab.com/jenifael.gingras/euler-golang/math"
import "gitlab.com/jenifael.gingras/euler-golang/fibo"

func main() {
    // Gérer via args quel probleme resoudre

    // Prob 1
    // sum := math.SumToN(1000)
    // fmt.Printf("La somme est: %v", sum)

    // Prob 2
    sum := fibo.SumFibo(4e6)
    fmt.Printf("La somme est: %v", sum)
}
