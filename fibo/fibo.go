package fibo

func SumFibo(limit int) int {
    sum := 0
    for fibo := range Fibo(limit) {
        if IsEven(fibo) {
            sum += fibo
        }
    }

    return sum
}

// Tentative naïve d'écrire un générateur de la suite de fibo
func Fibo(limit int) chan int {
    ch := make(chan int)

    go func() {
        first, second, next := 1, 1, 0
        for {
            if first >= limit {
                break
            }

            ch <- first

            next = first + second
            first = second
            second = next

        }
        close(ch)
    }()

    return ch
}


func IsEven(n int) bool {
    return n % 2 == 0
}
