package fibo

import "testing"
import "github.com/stretchr/testify/assert"

func TestFibo(t *testing.T) {
	assert := assert.New(t)
	expected := []int{1, 1, 2, 3, 5, 8}
	idx := 0

	for got := range Fibo(10) {
		assert.Equal(expected[idx], got)
		idx++
	}

	assert.Equal(len(expected), idx)
}

func TestIsEven(t *testing.T) {
	assert := assert.New(t)

	got := IsEven(2)

	assert.True(got)
}

func TestSumFibo(t *testing.T) {
	assert := assert.New(t)

	got := SumFibo(10)

	assert.Equal(10, got)
}
