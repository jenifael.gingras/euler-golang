package math

func IsAMultiple(n int) bool {
    return n % 3 == 0 || n % 5 == 0
}

func SumToN(n int) int {
    sum := 0

    for i := 0; i < n; i++ {
        if IsAMultiple(i) {
            sum += i
        }
    }

    return sum
}
