package math

import "testing"
import "fmt"
import "github.com/stretchr/testify/assert"

type isATest struct {
    n int
}

var isA = []isATest{
    {3},
    {5},
    {6},
    {9},
    {10},
}

var isNot = []isATest{
    {1},
    {2},
    {4},
    {7},
    {8},
}

func TestIsAMultiple(t *testing.T) {
    assert := assert.New(t)

    for _, test := range isA {
        assert.True(IsAMultiple(test.n), fmt.Sprintf("%v should be a valid multiple.", test.n))
    }
}

func TestNotIsAMultiple(t *testing.T) {
    assert := assert.New(t)

    for _, test := range isNot {
        assert.False(IsAMultiple(test.n), fmt.Sprintf("%v should not be a valid multiple.", test.n))
    }
}

func TestSumToN(t *testing.T) {
    assert := assert.New(t)

    got := SumToN(10)

    assert.Equal(23, got)
}
